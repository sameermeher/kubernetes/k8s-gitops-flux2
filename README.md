## Install Flux (Mac OS)
`brew install fluxcd/tap/flux`

## Verify that your cluster satisfies the prerequisites with:
`flux check --pre`

## Bootstrap Command -
`EXPORT GITLAB_USER='sameer-kubernetes-stack'`

`flux bootstrap gitlab \
  --components-extra=image-reflector-controller,image-automation-controller \
  --owner=$GITLAB_USER \
  --repository=gitops-flux \
  --branch=master \
  --path=clusters/staging \
  --read-write-key \
  --token_auth \
  --personal`

## Check Flux bootstrap status
`kubectl -n flux-system get gitrepository`
`flux get sources git`

## Flux to pull the manifests from Git and upgrade
`flux reconcile source git flux-system`

## Verify that the controllers have been upgrade with
`flux check`

`To check all Flux commands`
`flux`

## Kustomize
`flux get kustomizations`
`flux reconcile kustomization realtimeapp-dev` # To reconcile a specific deployment without waiting for sync interval

## Configure Monitoring
`flux create source git monitoring --interval=30m --url=https://github.com/fluxcd/flux2 --branch=main --export > ./dev-cluster/monitor-source.yaml`

`flux create kustomization monitoring --source=monitoring --path="./manifests/monitoring" --prune=true --interval=1h --health-check="Deployment/prometheus.flux-system" --health-check="Deployment/grafana.flux-system" --health-check-timeout=2m --export > ./dev-cluster/monitor-kustomizations.yaml`

`kubectl -n flux-system port-forward svc/grafana 3000:3000`

## Helm
`flux get helmreleases`
`flux reconcile helmrelease dev-app` # To reconcile a specific deployment without waiting for sync interval

## Alerting
`kubectl -n flux-system create secret generic slack-url --from-literal=address=https://hooks.slack.com/services/<<GENERATED_SLACK_HOOK_ID>>`

`flux get alert-providers`
`flux get alerts`

## ImageAutomation Updates
`flux get image repository customer-service`
`flux get image policy customer-service`
`flux get images all --all-namespaces`

## Clean up Flux
`flux uninstall --namespace=flux-system`
